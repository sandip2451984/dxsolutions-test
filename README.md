==========
Setup dxsolutions-test App
==========


1> first change database credential in parameter.yml

2> run php composer install command to install vendor.

3> run below command to setup database from entity.

    php app/console doctrine:database:drop --force
    php app/console doctrine:database:create
    php app/console doctrine:schema:create

4> create admin user using below command for access job/location page (its accessible by admin only)

	php bin/console fos:user:create username --super-admin

5> working functionality url is like below.

    /         (homepage)

        -  menu with link of job and locations

    /admin/location

        - add/edit/list/delete functionality of location.

    /admin/job
	    - add/edit/list/delete functionality of job.

5> API Integration with OAuth2 credential.

	- first you should need to run below command to generate cliend_id and client_secretkey in backend database (copy client_id and client_secret with generated below command).
	
		php app/console vp:oauth-server:client-create  --redirect-uri="www.test.com"  --grant-type="client_credentials"
	
	- run below url with pass client_id and client_secret for generate token to access api
	
		/oauth/v2/token?client_id=1_4z3513husd0cg4c000cgkkgkwsg40sg4oogc0404gswkwgcgwk&client_secret=2ahef5vlkpc0w0ckw8oscs4wg0kcwgc0ggsscogkwc8884s4ws&grant_type=client_credentials

    - You can call api using below url with pass access_token (use your generated access_token in past url). You can get each job+location info with below url.
	
		/api/details?access_token=MzcyMjJmMmZkMTVhMjY0ZmQwOTZhZjgyMjhhMWFkMDY0MGEyZWJiNzAyMTBlYzQ0NmMyNTYwOGRjZjk0ZmIyMg

