<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="locations")
 *
 * Defines the properties of the locations entity to represent the location of the job.
 *
 * @author Sandip Limbachiya <sandip2451984@gmail.com>
 */
class Locations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     * @Assert\NotBlank(message="location.name.blank")
     */
    private $name;

    /**
     * @var job
     *
     * @ORM\OneToMany(targetEntity="Job", mappedBy="locations")
     */
    protected $job;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string")
     * @Assert\NotBlank(message="location.city.blank")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string")
     * @Assert\NotBlank(message="location.country.blank")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;


    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * get Id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * get name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * set name
     * 
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * get job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * set job
     * 
     * @param Job $job
     */
    public function setJob(Job $job)
    {
        $this->job = $job;
    }

    /**
     * get city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * set city
     * 
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * get country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * set country
     * 
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * get address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * set address
     * 
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}