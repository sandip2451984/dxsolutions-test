<?php

// src/AppBundle/Security/Firewall/ExceptionListener.php
namespace AppBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Firewall\ExceptionListener as BaseExceptionListener;

/**
 * Exception listener for check exception of without api call
 */
class ExceptionListener extends BaseExceptionListener
{
    /**
     * set target path
     * 
     * @param Request $request
     * 
     * @return
     */
    protected function setTargetPath(Request $request)
    {
        // Do not save target path for XHR requests
        // You can add any more logic here you want
        // Note that non-GET requests are already ignored
        if ($request->isXmlHttpRequest()) {
            return;
        }

        parent::setTargetPath($request);
    }
}