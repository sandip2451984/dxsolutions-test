<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\User;
use AppBundle\Entity\AccessToken;

/**
 * Api controller.
 *
 * @Route("api")
 */
class ApiController extends FOSRestController
{
    /**
     * Lists all job+location entities.
     *
     * @Route("/details", name="api_index")
     */
    public function indexAction()
    {
        $accessToken  = $this->get('security.token_storage')->getToken();

        if ($accessToken) {
            $tokenManager = $this->get('fos_oauth_server.access_token_manager.default');
            $token = $tokenManager->findTokenByToken($accessToken->getToken());
            if ($token instanceof AccessToken) {
                if (time() < $token->getExpiresAt()) {
                    $repository = $this->getDoctrine()->getRepository('AppBundle:Job');
                    $data = $repository->findAll();
                } else {
                    $data = array('expired' => 'token expired! please select another one!');
                }
            } else {
                $data = array('Token Not Found' => 'token not found! please generate token first using oAuth2!');
            }
        } else {
            $data = array('Token Not Found' => 'token not found! please generate token using oAuth2 and try again!');
        }
        $view = $this->view($data);
        $view->setFormat('json');
        return $this->handleView($view);
    }
}
